const fs = require('fs');

const express = require('express');
const cors = require('cors')

const app = express();

app.use(cors())

app.get('*', (req, res) => {
    fs.readFile(`./jsons${req.path}.json`, (err, json) => {
        if (err) {
            return res.json(err)
        }

        let obj = JSON.parse(json);

        res.json(obj);
    })
});

app.listen(process.env.PORT || 3000)